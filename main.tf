resource "helm_release" "vault" {
  depends_on = [
    kubernetes_namespace_v1.vault
  ]
  name       = "vault"
  repository = "https://helm.releases.hashicorp.com"
  chart      = "vault"
  namespace  = kubernetes_namespace_v1.vault.metadata.0.name
  timeout    = 1200
  version    = "0.22.0"
}
