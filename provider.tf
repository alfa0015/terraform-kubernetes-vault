terraform {
  required_version = "~> 1.2"
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.23"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "~> 2.14"
    }
    helm = {
      source = "hashicorp/helm"
      version = "~> 2.7"
    }
  }
}

provider "digitalocean" {
  token = var.do_token
}

provider "kubernetes" {
  host  = data.digitalocean_kubernetes_cluster.cluster.endpoint
  token = data.digitalocean_kubernetes_cluster.cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(
    data.digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate
  )
}

provider "helm" {
  kubernetes {
    host  = data.digitalocean_kubernetes_cluster.cluster.endpoint
    token = data.digitalocean_kubernetes_cluster.cluster.kube_config[0].token
    cluster_ca_certificate = base64decode(
      data.digitalocean_kubernetes_cluster.cluster.kube_config[0].cluster_ca_certificate
    )
  }
}