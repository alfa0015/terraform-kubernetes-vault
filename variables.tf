variable "do_token" {
  type = string
  description = "token to authenticate with digital ocean"
}
variable "region" {
  type = string
  description = "region to deploy cluster"
  default = "nyc1"
}
variable "cluster_name" {
  type = string
  description = "name to kubernetes cluster"
  default = "demo"
}

data "digitalocean_kubernetes_cluster" "cluster" {
  name = var.cluster_name
}